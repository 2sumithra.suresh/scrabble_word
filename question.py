<<<<<<< HEAD
Let's start coding here

def generate_neighbors(current_word, allowed_words):
    neighbors = []
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    word_list = list(current_word)
    
    for i in range(len(word_list)):  # For each character in the word
        original_char = word_list[i]
        for char in alphabet:  # For each letter in the alphabet
            if original_char != char:  # Change the character if it's different
                word_list[i] = char
                new_word = ''.join(word_list)
                if new_word in allowed_words:  # Check if the new word is valid
                    neighbors.append(new_word)
        word_list[i] = original_char  # Restore original character
    return neighbors

def find_word_ladder(start_word, end_word, word_file):
    allowed_words = load_words(word_file)
    if end_word not in allowed_words:
        return []

    queue = [[start_word]]
    visited = set([start_word])
    
    while queue:
        current_path = queue.pop(0)
        current_word = current_path[-1]
        
        # Debugging print statements
        print(f"Current path: {' -> '.join(current_path)}")
        
        if current_word == end_word:
            return current_path
        
        for neighbor in generate_neighbors(current_word, allowed_words):
            if neighbor not in visited:
                visited.add(neighbor)
                queue.append(current_path + [neighbor])
                
                # Debugging print statements
                print(f"Visited: {visited}")
                print(f"Queue: {queue}")
    
    return []
=======
def loading_words(file_path): 
     with open (file_path, 'r' ) as file :
          return set(word.strip().upper() for word in file if len(word.strip()) == 4)

>>>>>>> fc932610ad4d67d86636b6adc70f968a01d37dde
